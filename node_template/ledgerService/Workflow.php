<?php

class Workflow {
  public $id;
  public $label;
  public $summary;
  private $states;
  private $transaction;

  const FILEPATH = './workflows';

  const RELATION_PAYEE = 'payee';
  const RELATION_PAYER = 'payer';
  const RELATION_AUTHOR = 'author';
  const RELATION_ADMIN = '-admin-'; // not sure about this one.

  function __construct(stdClass $info, Transaction $transaction) {
    $this->id = $info->id;
    $this->label = $info->label;
    $this->summary = $info->summary;
    $this->states = (array)$info->states;
    $this->transaction = $transaction;
  }

  /**
   *
   * @param string $id
   * @param Transaction $transaction
   * @return \static\
   *
   * @todo At some point we should check that the transaction scope is not larger than the workflow's scope.
   */
  function create($id, Transaction $transaction) {
     foreach (static::getAll() as $nName => $wfs) {
       foreach ($wfs as $wf) {
         if ($wf->id == $id) return new static ($wf, $transaction);
       }
     }
    cc_violation(UnknownWorkflowViolation::create($id));
  }

  /**
   * Get all the operations the current user can do on the current transaction.
   *
   * @param stdClass $transaction
   * @return array
   */
  function getActions(): array {
    $actions = [];
    $relatives = $this->getMyRelations();
    $state = (array)$this->states[$this->transaction->state];
    foreach ($state as $target_state => $info) {
      $permitted = $info->signatories;
      $permitted[] = '-admin-';
      if (array_intersect($relatives, $permitted)) {
        $actions[$target_state] = $info->label;
      }
    }
    return $actions;
  }

  function canSign(string $target_state) : bool {
    $permitted = $this->states[$this->transaction->state]->{$target_state}->signatories;
    $permitted[] = '-admin-';
    if (array_intersect($this->getMyRelations(), $permitted)) {
      return TRUE;
    }
    cc_violation(WorkflowViolation::create($this->uuid, $target_state, $this->id));
  }

  /**
   * Determine the relationship(s) of the authenticated user to the current
   * transaction
   * @return array
   *   A list of account names.
   */
  function getMyRelations() : array {
    global $admin, $config;
    $relations = [];
    $u = fetch_policy($_SESSION['user'], 'full', TRUE);
    if($u->name == $admin or $u->url) { // All remote accounts are given the benefit of the doubt.
      $relations[] = Workflow::RELATION_ADMIN;
    }
    if($u->name == $this->transaction->payee->localName) {// taken from the first entry.
      $relations[] = Workflow::RELATION_PAYEE;
    }
    elseif ($u->name == $this->transaction->payer->localName) {
      $relations[] = Workflow::RELATION_PAYER;
    }
    if ($u->name == $this->transaction->author) {// todo this might be ambiguous.
      $relations[] = Workflow::RELATION_AUTHOR;
    }
    // Todo test for blog relations on the other entries.
    return $relations;
  }


  /**
   * Convert the transaction relations to account names
   * @param stdClass $transaction
   * @param string $relation
   * @return string
   */
  function getAccountFromRelation(string $relation) {
    switch ($role) {
      case RELATION_PAYEE:
        return $this->transaction->payee;
      case RELATION_PAYER:
        return $this->transaction->payer;
      case RELATION_AUTHOR:
        return $this->transaction->author;
      case RELATION_ADMIN:
        return '-admin-';
    }
  }


  /**
   * Get the workflows from all rootwards nodes, replacing them with localised
   * (translated) versions, using the hash to identify them
   *
   * @return array
   *   Translated workflows, keyed by the rootwards node name they originated from
   *
   * @todo If not cached this will cause a drag on performance
   */
  static function getAll() : array {
    global $node_name, $orientation;
    $local = $rootwards = $remote = [];
    // get the local workflows
    if (file_exists('workflows.json')) {
      $json = file_get_contents('workflows.json');
      foreach (json_decode($json) as $id => $wf) {
        if ($wf->active) {
          $local[static::wfHash($wf)] = $wf;
        }
      }
    }
    // get rootwards workflows.
    if ($account = $orientation->getRootwardsAccount()) {
      $rootwards = LedgerRequester::create($account)->getWorkflows();
      foreach ($rootwards as $nName => $wfs) {
        foreach ($wfs as $wf) {
          if ($wf->active) {
            $remote[$nName][static::wfHash($wf)] = $wf;
          }
        }
      }
    }
    if ($local) {
      // Now compare the hashes, and where similar, replace the rootwards one with the local translation.
      foreach ($remote as $nName => $wfs) {
        foreach ($wfs as $hash => $wf) {
          if (isset($local[$hash])) {
            $remote[$nName][$hash] = $local[$hash];
            unset($local[$hash]);
          }
        }
      }
    }
    if ($local) {
      $remote[$node_name] = $local;
    }
    if (empty($remote)) {
      cc_failure(MissingWorkflowError::create());
    }
    foreach ($remote as $nName => &$wfs) {
      foreach ($wfs as $hash => $wf) {
        $all[$nName][$wf->id] = $wf;
      }
    }
    return $all;
  }


  static function wfHash(stdClass $wf) {
    foreach ($wf->states as $source_name => $source_state) {
      foreach ((array)$source_state as $target_name => $target_state) {
        $hashable_states[$source_name][$target_name] = $target_state->signatories;
      }
    }
    return md5(json_encode($hashable_states));
  }

}


