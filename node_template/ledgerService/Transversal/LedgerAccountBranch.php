<?php

/**
 * Class representing an account
 */
abstract class LedgerAccountBranch extends LedgerAccountRemote {

  /**
   *
   * @param stdClass $policy
   *   converted json from the PolicyService Account class
   * @param string $given_path
   */
  function __construct(stdClass $policy, string $given_path = '') {
    parent::__construct($policy, $given_path);
    global $node_name;
    if ($given_path) {
      $parts = explode('/', $given_path);
      $pos = array_search($this->localName, $parts);
      if ($branchwards = array_slice($parts, $pos + 1)) {
        $tail = implode('/', $branchwards);
        $this->relative .= "/$tail";
      }
    }
  }

}

/**
 * Class representing an account
 */
final class LedgerAccountUpstreamBranch extends LedgerAccountBranch {

}

/**
 * Class representing an account
 */
final class LedgerAccountDownstreamBranch extends LedgerAccountBranch {

}

