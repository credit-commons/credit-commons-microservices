<?php

/**
 * Implements the client to ledger and inter-ledger communication of the credit
 * commons API.
 *
 * Handles requests & responses to the downstream ledger.
 */
class LedgerRequester extends BaseRequester {

  const SUBDOMAIN = '';

  /**
   * Saves calling the global of the same name.
   * @var LedgerAccountRemote
   */
  private $downstreamAccount;

  /**
   * @param LedgerAccountRemote $dest_account
   */
  function __construct(LedgerAccountRemote $dest_account) {
    $this->downstreamAccount = $dest_account;
    parent::__construct($dest_account);
  }


  /**
   * Return a requester object
   * @param LedgerAccountRemote $dest
   *   Of the remote ledger
   * @return Requester
   */
  static function create(LedgerAccountRemote $dest) {
    static $cached;
    $key = isset($dest) ? $dest->localName : 'local';
    if (!isset($cached[$key])) {
      $cached[$key] = new LedgerRequester($dest);
    }
    return $cached[$key];
  }

  /**
   * {@inheritDoc}
   */
  protected function request(string $endpoint = '') :array {
    global $node_name;
    // The dev client doesn't have a node name, and shouldn't authenticate as a node
    $this->setHeader('Upstream-Hash', $this->downstreamAccount->getLastHash());
    $this->setHeader('Upstream-Node', $node_name);
    $this->accept(400);
    list($code, $result) = parent::request($endpoint);
    if ($code == 400) {
      if ($result) {
        cc_violation($this->decodeJsonError($result));
      }
      cc_failure(UnexpectedResultFailure::create($code, $this->fullUrl, $result, '400 error returned without Error Object'));
    }
    return [$code, $result];
  }

  /**
   * @return array
   *   The response code, and if applicable, the details.
   */
  function handshake() : array {

    $this->accept(200, 409);
    $this->setTimeout(1);
    list($code, $details) = $this->request();

    return [$code, (array)$details];
  }


  /**
   * Add rows with the business logic and return rows with converted values.
   * @param Transaction $transaction
   */
  function buildValidateTransaction(Transaction $transaction) {
    global $orientation;
    if ($orientation->upstreamAccount and $this->downstreamAccount->localName == $orientation->upstreamAccount->localName) {
      // This happened frequently during development causing a lot of timeouts and memory errors.
      cc_failure(MiscFailure::create('Attempted to request upstream.'));
    }
    list($code, $rows) = $this->setMethod('POST')
      ->accept(201)
      ->setBody($transaction)
      ->request('buildrelay');
    foreach ($rows as $row) {
      // Create a Rootward entry, convert the quant to local, and put in Ledger::newEntries
      $row->author = $this->downstreamAccount->localName;
      $transaction->entries[] = Entry::create($row, FALSE)->additional();
    }
  }

  /**
   * @param array $fields
   * @return array
   */
  function filter(array $fields) : array {
    // Send only valid fields
    $valid = ['payer', 'payee', 'involving', 'before', 'after', 'uuid', 'quantity', 'description', 'state', 'type', 'is_primary', 'full'];
    $fields = array_intersect_key(array_filter($fields), array_flip($valid));
    foreach ($fields as $key =>$value) {
      $this->addQueryParam($key, $value);
    }
    list($code, $results) = $this->accept(200)->request('filter');

    if ($code == 200) {
      return (array)$results;
    }
    return [];
  }

  /**
   * Get a list of balances and times for an account.
   * @param string $name
   * @return array
   */
  function getHistory(string $name, int $samples = 0) : array {
    cc_message("Getting history for account $name");
    $path = 'history/'.$name;
    if ($samples) {
      $path .= '/'.$samples;
    }
    list($code, $result) = $this
      ->accept(200, 404)
      ->request($path);
    if ($code == 404) {
      cc_violation(UnresolvedAccountnameViolation::create($name));
    }
    return (array)$result;
  }

  /**
   * @param string $name
   * @return array
   *   the name of the account retrieved and the stats
   */
  function getStats(string $name) : array {
    list($code, $result) = $this
      ->accept(200, 404)
      ->request('account/'.$name);
    if ($code == 404) {
      cc_violation(UnresolvedAccountnameViolation::create($name));
    }
    return $result;
  }

  /**
   * Get a list of all accounts on the ledger and all parents.
   *
   * @param bool $details
   * @param bool $deep
   * @return array
   */
  function accounts(bool $details = TRUE, bool $tree = FALSE) : array {
    list($code, $result) = $this
      ->accept(200)
      ->addQueryParam('details', $details)
      ->addQueryParam('tree', $tree)
      ->request('accounts');
    return (array)$result;
  }

  /**
   * @param string $uuid
   * @param string $target_state
   */
  function transactionChangeState(string $uuid, string $target_state) {
    list($code, $body) = $this->setMethod('PATCH')
      ->accept(201, 403, 404)
      ->request("transaction/$uuid/$target_state");
    if ($code == 403) {
      cc_violation(WorkflowViolation::create($uuid, $target_state, Transaction::loadByUuid($uuid)->type));
    }
    elseif ($code == 404) {
      cc_violation(DoesNotExistViolation::create($uuid));
    }
  }

  /**
   * Get the supported workflows, localised
   * @return array
   */
  public function getWorkflows() : array {
    list($code, $workflows) = $this->accept(200)->request('workflows');
    return (array)$workflows;
  }
}
