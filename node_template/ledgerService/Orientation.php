<?php

class Orientation {

  public $downstreamAccount;
  public $upstreamAccount;
  public $localRequest;
  private $rootwardsAccountName;
  private $rootwardsAccount;

  const REQUESTMODE = FALSE;
  const RESPONSEMODE = TRUE;

  /**
   * FALSE for request, TRUE for response mode
   * @var Bool
   */
  public $requestResponseMode;

  function __construct(LedgerAccountRemote $account = NULL) {
    global $config;
    $this->requestResponseMode = self::REQUESTMODE;
    $this->upstreamAccount = $account;
    $this->rootwardsAccountName = $config['bot_account'];
    if ($this->rootwardsAccountName and $this->upstreamAccount) {
      if ($this->rootwardsAccountName == $this->upstreamAccount->localName) {
        $this->rootwardsAccount = $this->upstreamAccount;
      }
    }
  }


  /**
   * Any remote account which isn't the upstreamAccount is marked as the downstream account
   */
  function addAccount(LedgerAccount $acc) : void {
    if ($acc instanceOf LedgerAccountRemote) {
      if (!$this->upstreamAccount or $acc->localName != $this->upstreamAccount->localName) {
        $this->downstreamAccount = $acc;
        if ($this->rootwardsAccountName and $this->rootwardsAccountName == $acc->localName) {
          $this->rootwardsAccount = $this->upstreamAccount;
        }
      }
    }
  }

  function getDownstreamRequester() {
    if ($this->downstreamAccount) {
      return LedgerRequester::create($this->downstreamAccount);
    }
  }


  function getRootwardsAccount() {
    if (!$this->rootwardsAccount and $this->rootwardsAccountName) {
      include_transversal_classes();
      // Load this account ensuring it is the LedgerAccountRemote class by naming it
      $name = $this->rootwardsAccountName . '/'.$node_name;
      $policy = fetch_policy($this->rootwardsAccountName, 'full', TRUE);
      $this->rootwardsAccount = new LedgerAccountBoT($policy, $name);
    }
    return $this->rootwardsAccount;
  }

  function orientToRoot() : bool {
    if ($this->rootwardsAccountName) {
      $this->rootwardsAccount = $this->getRootwardsAccount();
      $this->downstreamAccount = $this->rootwardsAccount;
    }
    return isset($this->rootwardsAccount);
  }

  function isUpstreamBranch() {
    if ($this->rootwardsAccountName) {
      if ($ups = $this->upstreamAccount) {
        if ($ups->localName <> $this->rootwardsAccountName)
          return TRUE;
      }
      else {
        return TRUE;
      }
    }
  }

  /**
   * Ledger orientation functions. Used for converting transactions to send.
   */
  // return TRUE or FALSE
  function goingDownstream() : bool {
    return $this->downstreamAccount && $this->requestMode() && !$this->localRequest;
  }
  function goingUpstream() : bool {
    return $this->upstreamAccount && $this->responseMode() && !$this->localRequest;
  }

  // return TRUE, FALSE
  function goingRootwards() {
    return $this->rootwardsAccount and (
      $this->downstreamAccount == $this->rootwardsAccount && $this->requestMode()
      or
      $this->upstreamAccount == $this->rootwardsAccount && $this->responseMode()
    ) && !$this->localRequest;
  }

  function upstreamIsRootwards() : bool {
    return $this->rootwardsAccountName == $this->upstreamAccount->localName;
  }

  // Check if we are going upstream or downstream
  function requestMode() : bool {
    return $this->requestResponseMode == static::REQUESTMODE;
  }
  function responseMode() : bool {
    return $this->requestResponseMode == static::RESPONSEMODE;
  }

  function adjacentAccount() {
    if ($this->requestMode()) {
      return $this->downstreamAccount;
    }
    else{
      return $this->upstreamAccount ?? 'client';
    }
  }

  /**
   * Check all the adjacent nodes are online and the ratchets match and return results.
   * @return array
   */
  function handshake() : array {
    global $node_name;
    $results = [];
    if ($this->upstreamAccount) {
      header('Node-name: '.$node_name);
    }
    else {
      $active_policies = PolicyRequester::create()->filter(['status' => 1]);
      foreach ($active_policies as $policy) {
        if (isset($policy->url)) {
          //Make sure we load the remote version by giving a path longer than 1 part.
          $ledgerAccount = LedgerAccount::create("$node_name/$policy->name", TRUE);
          list($code) = LedgerRequester::create($ledgerAccount)->handshake();
          $results[$code][] = $policy->name;
        }
      }
    }
    return $results;
  }

}
