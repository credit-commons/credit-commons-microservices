<?php
require 'db.php';
require '../common.php';
header('Node-name: '.$node_name);
$headers = getallheaders();
ledger_journal(join("\n", $headers));
if ($headers['Content-Type'] == 'text/html') : ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
  <head>
    <title>Credit Commons client</title>
  </head>
  <body>
    <p>This is a Credit Commons node called '<?php print $node_name?>'.</p>
    <p>It consists of microservices each on its own subdomain of <?php print $_SERVER['SERVER_NAME']; ?> which can only be accessed through its REST API
      <?php if (is_dir('../../ccclient')) print ", however this server may have a <a href=\""."http://".str_replace($node_name, 'ccclient', $_SERVER['SERVER_NAME'])."\">developer client</a>";?>.</p>
    <p>This url could be used as a public front page for the node, showing stats etc.
    <p>For more information please see <a href="http://creditcommons.net">creditcommons.net</a>.</p>
  </body>
</html>
<?php exit; endif;
/**
 * ledgerService
 *
 * This service is the only service which may be called from another node.
 */

require 'Orientation.php';
require 'LedgerAccount.php';
require 'Entry.php';
require 'violations.php';
require 'Transaction.php';
require 'Workflow.php';
require 'requesters/BaseRequester.php';
require 'requesters/PolicyRequester.php';
require 'requesters/BlogicRequester.php';
require 'requesters/LedgerRequester.php';


authenticateUpstream($headers, $_SERVER['REQUEST_URI']);

// READ FUNCTIONS
if ($endpoint == '') {
  cc_ledger_response(200, $orientation->handshake());
}
if ($endpoint == 'workflows') {
  cc_response(200, Workflow::getAll());
}
if (empty($_SESSION['user'])) {
  cc_response(401);
}

if ($method == 'GET') {
  check_private();
  if ($endpoint == 'filter') {
    parse_str($_SERVER['QUERY_STRING'], $params);
    $uuids = Transaction::filter($params);
    if ($uuids and !empty($params['full'])) {
      foreach ($uuids as $key => $uuid) {
        $transactions[$uuid] = Transaction::loadByUuid($uuid);
      }
      // Question = Should we make an internal class to pass transactions between nodes?
      cc_ledger_response(200, $transactions);
    }
    else {
      cc_ledger_response(200, $uuids);
    }
  }
  elseif($endpoint == 'accounts') {
    parse_str($_SERVER['QUERY_STRING'], $params);
    if ($params['tree'] and $orientation->orientToRoot()) {
      $class = 'LedgerAccountRemote';
    }
    else {
      $class = 'LedgerAccount';
    }
    cc_response(200, $class::getAllTradeStats((bool)$params['details']));
  }
  elseif ($endpoint == 'account') {
    $params = arg(-1);
    // This account could be remote.
    $account = LedgerAccount::create(implode('/', $params), FALSE);
    cc_response(200, $account->getTradeStats());

  }
  elseif ($endpoint == 'history') {
    $params = arg(-1);
    $samples = 0;
    if (is_numeric(end($params))) {
      $samples = array_pop($params);
    }
    $account = LedgerAccount::create(implode('/', $params), FALSE);
    cc_response(200, $account->getHistory($samples));
  }
}
// Create a new transaction
elseif ($method == 'POST') {
  $post = credcom_json_input();
  if ($endpoint == 'build') {
    $transaction = Transaction::createFromClient($post);
  }
  elseif ($endpoint == 'buildrelay') {
    // There is always an upstream node.
    include_transversal_classes();
    $transaction = TransversalTransaction::createFromUpstream($post);
  }

  $transaction->buildValidate();
  $transaction->writeValidatedToTemp();
  cc_ledger_response(201, $transaction);
}
// Change the state of an existing transaction
elseif ($target_state = arg(3) and $method == 'PATCH') {
  // validate input
  $valid_states = [
    TransactionInterface::STATE_PENDING,
    TransactionInterface::STATE_COMPLETED,
    TransactionInterface::STATE_ERASED
  ];
  $uuid = arg(2);
  if (!in_array($target_state, $valid_states)) {
    cc_response(400, "Invalid workflow type.");
  }
  $transaction = Transaction::loadByUuid($uuid);
  // Ensure that transversal transactions are being manipulated only from their
  // end points, not an intermediate ledger
  if (!$orientation->upstreamAccount and $transaction->payer->url and $transaction->payee->url) {
    cc_violation(IntermediateLedgerViolation::create());
  }

  $transaction->changeState($target_state);
  cc_ledger_response(201);
}

cc_ledger_response(404, "Could not match $method input to route", "Could not match $method input to route");


if (!function_exists('getallheaders')) {
  function getallheaders() {
    $headers = [];
    foreach ($_SERVER as $name => $value) {
      if (substr($name, 0, 5) == 'HTTP_') {
        $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
      }
    }
    return $headers;
  }
}

/**
 *
 * @param LedgerAccount $account1
 * @param LedgerAccount $account2
 * @return bool
 *
 * @todo shouldn't this reference workflow?
 */
function hasCreatePermission (LedgerAccount $account1, LedgerAccount $account2) : bool {
  // A transaction can only be created:
  // When the current user is either payer or payee or admin
  if ($account1->localName == $_SESSION['user'] or $account2->localName == $_SESSION['user']) {
    return TRUE;
  }
  // When the current user is admin
  return $_SESSION['user'] == '-admin-';
}


function include_transversal_classes() {
  require_once 'Transversal/TransversalTransaction.php';
  require_once 'Transversal/TransversalEntry.php';
  require_once 'Transversal/LedgerAccountRemote.php';
  require_once 'Transversal/LedgerAccountBoT.php';
}


/**
 * Prevent the BoT_account from reading the ledger.
 */
function check_private() {
  global $config;
  // If the request is coming from the __BoT__ account then check privacy settings
  if ($config['private'] and $_SESSION['user'] == $config['bot_account']) {
    cc_response(403);
  }
}

function cc_violation(ErrorInterface $violation) {
  ledger_journal($violation, 'clientError');
  cc_response(400, $violation);
}
function cc_failure(CCError $error) {
  ledger_journal($body, 'serverError');
  // This error code is not used for anything else. Called in BaseRequester::Request()
  cc_response(515, $error);
}


function ledger_journal($message, $type = 'request') {
  global $method;
  $message = Db::connect()->real_escape_string($message);
  $path = $_SERVER['REQUEST_URI'];
  Db::query("INSERT into log (type, http_method, path, message) VALUES ('request', '$method', '$path', '$message')");
}

/**
 * Switch the ledger mode before converting the response to json.
 *
 * @param int $code
 * @param Mixed $body
 */
function cc_ledger_response(int $code, $body = NULL) {
  global $orientation;
  $orientation->requestResponseMode = Orientation::RESPONSEMODE;
  cc_response($code, $body);
}


function authenticateUpstream(array $headers) {
  global $orientation, $dev_mode, $admin;
  // Find out where the request is coming from, check the hash if it is another
  // node, and set up the session, which should persist accross the microservices.
  if (isset($headers['User-Agent']) and $headers['User-Agent'] == 'Credit-Commons') { // @todo write this into the api
    if ($upstreamAccount = checkUpstreamHash($headers['Upstream-Node'], $headers['Upstream-Hash']??'')) {
      // This is a temporary placeholder for an authenticated user mechanism
      $_SESSION['user'] = $upstreamAccount->localName;
    }
    else {
      cc_log($row, "Integrity-Error on $node_name");
      cc_violation(HashMismatchViolation::create($node_name));
    }
  }
  elseif (isset($headers['Cc-Enduser'])) {
    // @todo Check that the CC-user is a real account
    $_SESSION['user'] = $headers['Cc-Enduser'];
    cc_message('Using node as '.$_SESSION['user']);
  }
  elseif ($dev_mode) {
    $_SESSION['user'] = $admin;
  }
  $orientation = new Orientation(@$upstreamAccount);
}

/**
 *
 * @param string $node_name
 * @param int $id
 * @param string $hash
 * @return bool
 *   TRUE if the hash matches
 */
function checkUpstreamHash(string $ups_node, string $hash) : LedgerAccountRemote {
  global $config, $loadedAccounts, $node_name;
  if ($hash) {
    $query = "SELECT TRUE FROM hash_history WHERE acc = '$ups_node' AND hash = '$hash' ORDER BY id DESC LIMIT 0, 1";
    if (!Db::query($query)->fetch_row()) {
      cc_violation(HashMismatchViolation::create());
    }
  }
  // if no hash is given and no has is in the db then allow the connection.
  else{
    $row = Db::query("SELECT count(hash) as hashes FROM hash_history")->fetch_row();
    if ($row->hashes) {
      cc_violation(HashMismatchViolation::create());
    }
  }
  include_transversal_classes();
  $policy = fetch_policy($ups_node);
  if ($config['bot_account'] == $ups_node) {
    require_once 'Transversal/LedgerAccountBoT.php';
    $account = new LedgerAccountUpstreamBoT($policy, $ups_node);
  }
  else {
    require_once 'Transversal/LedgerAccountBranch.php';
    $account = new LedgerAccountUpstreamBranch($policy, $ups_node);
  }
  $loadedAccounts["$ups_node/$node_name"] = $account;
  return $account;
}

/**
 *
 * @staticvar array $fetched
 * @param string $name
 * @param string $view_mode
 * @param bool $known
 *   If TRUE and the name is not found, throw an error.
 * @return type
 */
function fetch_policy(string $name, string $view_mode = 'full', bool $known = TRUE) {
  static $fetched = [];
  if (!isset($fetched[$view_mode][$name])) {
    $fetched[$view_mode][$name] = PolicyRequester::create()->fetch($name, $view_mode);
  }
  if ($known and empty($fetched[$view_mode][$name])) {
    cc_failure(MissingPolicyFailure::create($name));
  }
  return $fetched[$view_mode][$name];
}