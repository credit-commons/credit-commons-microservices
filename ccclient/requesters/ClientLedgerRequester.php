<?php

require '../node_template/ledgerService/violations.php';

/**
 * Handle requests & responses from the client to the ledger.
 */
class ClientLedgerRequester extends LedgerRequester {
  use ClientRequesterTrait;

  function __construct(string $other_domain) {
    $this->serviceUrl = $other_domain;
  }

  /**
   * @param string $name
   * @return array
   */
  function getStats(string $name) : array {
    if (!$name) {
      clientAddError('No account name provided');
      return '';
    }
    return parent::getStats($name);
  }

  /**
   * @param stdClass $entry
   * @return Transaction
   *   With either rows or violations
   */
  function buildNewTransaction(stdClass $entry) {
    @list($code, $transaction) = $this->setMethod('POST')
      ->accept(200, 201, 404)
      ->setBody($entry)
      ->request('build');
    if ($code == 200) {
      if (is_array($transaction)) {
        // don't know what the problem is.
        clientAddError ('array returned from ledgerservice/build');
        return;
      }
      $error_messages = $this->decodeJsonError($transaction);
      clientAddError($error_messages);
      return;
    }
    elseif ($code == 404) {
      clientAddError('Unable to find account');
      return;
    }
    elseif (isset($transaction->actions)) {
      $transaction->actions = (array)$transaction->actions;
    }
    return $transaction;
  }

  /**
   * @param string $uuid
   * @param string $target_state
   * @return bool
   */
  function transactionChangeState(string $uuid, string $target_state) : bool {
    @list($code, $result) = $this->setMethod('PATCH')
      ->accept(200, 201, 403, 404)
      ->request("transaction/$uuid/$target_state");
    if ($code == 200) {
      if (is_object($result)) {
        $errs = $this->decodeJsonError($result);
        clientAddError(implode("\n", $errs));
      }
      else {
        clientAddError("200 expected error object back from $this->fullUrl, got ".gettype($result));
        clientAddError($result);
      }
    }
    elseif ($code == 201) {
      return TRUE;
    }
    elseif ($code == 403) {
      clientAddError("Not allowed to change transaction state of $uuid");
    }
    elseif ($code == 404) {
      clientAddError("Transaction $uuid not found");
    }
    return FALSE;
  }


  function filter(array $fields) : array {
    // convert the checkbox to a boolean.
    if (isset($fields['full'])) {
      if ($fields['full'] == 'on')$fields['full'] = 1;
      elseif ($fields['full'] == 'off')$fields['full'] = 0;
    }
    $this->setMethod('GET');
    return parent::filter($fields);
  }
}
