#!/usr/bin/env bash
#
# Substitutes environment variables into nginx
# config based on those given by Docker
#
# @package: Credit Commons Microservices
# @since:   2020-10-18
#
##

echo "Generating Credit Commons node administrator host config..."

envsubst '$CC_NODE_ADMIN_HOSTNAME' < /config/node-admin.conf.tpl > /etc/nginx/sites-enabled/node-admin.conf

echo "Done!"
